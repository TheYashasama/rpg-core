# RPG Core
#### by Timo Neumann

#### Created for the 
## "RPG Core Combat Creator - Unity 2017 Compatible In C#" 
#### course on [Udemy](https://www.udemy.com). 

Course link: https://www.udemy.com/unityrpg/

Contents, cited from the courses **What will I learn** section:

  * Create core combat mechanics for melee, ranged and special attacks.
  * More advanced C# techniques such as interfaces, delegates, and co-routines.
  * Create pathfinding systems and patrol paths for enemies and NPCs.
  * Make a detailed level with terrain, enemies, triggers, lighting, particles and props.
  * Balance the player and enemy stats (eg. health, damage, movement, attack speed, and more).
  * Advanced game design, project management and code architecture strategies.


---

Copyright (c) 2018 Timo Neumann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
﻿using UnityEngine;
using UnityEngine.EventSystems;
using RPG.Characters;

namespace RPG.CameraUI
{
    public class CameraRaycaster : MonoBehaviour
    {
        public const float MAX_RAYCAST_DEPTH = 150f; // Hard coded value
        public const int WALKABLE_LAYER = 8;

        [SerializeField] Texture2D walkCursor = null;
        [SerializeField] Texture2D targetCursor = null;
        [SerializeField] Vector2 cursorHotspot = new Vector2(0, 0);

        public delegate void OnCursorOverWalkable(Vector3 position);
        public event OnCursorOverWalkable OnCursorOverWalkableObservers;

        public delegate void OnCursorOverEnemy(EnemyAI enemy);
        public event OnCursorOverEnemy OnCursorOverEnemyObservers;


        void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return; // Stop looking for other objects
            }
            DoRaycasts();
        }

        private void DoRaycasts()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // TODO maybe change to only Raycast()
            RaycastHit[] raycastHits = Physics.RaycastAll(ray, MAX_RAYCAST_DEPTH);

            // this order decides the priority
            if(RaycastForEnemy(raycastHits)) return;
            if(RaycastForWalkable(raycastHits)) return;
        }

        bool RaycastForEnemy(RaycastHit[] raycastHits)
        {
            foreach (RaycastHit hit in raycastHits)
            {
                EnemyAI enemyHit = hit.collider.gameObject.GetComponent<EnemyAI>();
                if (enemyHit)
                {
                    Cursor.SetCursor(targetCursor, cursorHotspot, CursorMode.Auto);
                    OnCursorOverEnemyObservers(enemyHit);
                    return true;
                }
            }
            return false;
        }

        bool RaycastForWalkable(RaycastHit[] raycastHits)
        {
            foreach (RaycastHit hit in raycastHits)
            {
                if (hit.collider.gameObject.layer == WALKABLE_LAYER)
                {
                    Cursor.SetCursor(walkCursor, cursorHotspot, CursorMode.Auto);
                    OnCursorOverWalkableObservers(hit.point);
                    return true;
                }
            }
            return false;
        }
    }
}
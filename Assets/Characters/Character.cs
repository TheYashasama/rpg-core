using UnityEngine;
using UnityEngine.AI;

using RPG.CameraUI;

namespace RPG.Characters
{
    [SelectionBase]
    public class Character : MonoBehaviour
    {
        private const float MOVE_THRESHOLD = 1f;

        [Header("Animator Settings")]
        [SerializeField]
        RuntimeAnimatorController animatorController;
        [SerializeField] AnimatorOverrideController animatorOverrideController;
        [SerializeField] Avatar animatorAvatar;
        [Range(0.1f, 1f)] [SerializeField] float animatorForwardCap = 1f;

        [Header("Audio Settings")]
        [Range(0f, 1f)]
        [SerializeField]
        float audioVolume;
        [Range(0f, 1f)] [SerializeField] float audioSpatialBlend;

        [Header("Capsule Collider Settings")]
        [SerializeField]
        Vector3 colliderCenter;
        [SerializeField] float colliderRadius;
        [SerializeField] float colliderHeight;

        [Header("Movement Settings")]
        [SerializeField]
        float movementSpeedMultiplier = 1f;
        [SerializeField] float movingTurnSpeed = 360;
        [SerializeField] float stationaryTurnSpeed = 180;
        [SerializeField] float animSpeedMultiplier = 1f;

        [Header("NavMesh Agent Settings")]
        [SerializeField]
        float stoppingDistance = 2f;
        [SerializeField] float navMeshAgentRadius = 0.2f;
        [SerializeField] float navMeshAgentSpeed = 0.2f;
        [SerializeField] bool navMeshAgentAutoBraking;

        NavMeshAgent navMeshAgent;
        Animator animator;
        Rigidbody rigidbodyComponent;

        float turnAmount;
        float forwardAmount;
        private bool isDead;

        public bool IsDead
        {
            get
            {
                return isDead;
            }
        }

        public void KillCharacter()
        {
            isDead = true;
        }

        private void Awake()
        {
            AddRequiredComponents();
        }

        void AddRequiredComponents()
        {
            animator = gameObject.AddComponent<Animator>();
            animator.runtimeAnimatorController = animatorController;
            animator.avatar = animatorAvatar;

            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.volume = audioVolume;
            audioSource.spatialBlend = audioSpatialBlend;

            CapsuleCollider collider = gameObject.AddComponent<CapsuleCollider>();
            collider.center = colliderCenter;
            collider.radius = colliderRadius;
            collider.height = colliderHeight;

            rigidbodyComponent = gameObject.AddComponent<Rigidbody>();
            rigidbodyComponent.constraints = RigidbodyConstraints.FreezeRotation;

            navMeshAgent = gameObject.AddComponent<NavMeshAgent>();
            navMeshAgent.updateRotation = false;
            navMeshAgent.updatePosition = true;
            navMeshAgent.stoppingDistance = stoppingDistance;
            navMeshAgent.radius = navMeshAgentRadius;
            navMeshAgent.speed = navMeshAgentSpeed;
            navMeshAgent.autoBraking = navMeshAgentAutoBraking;
        }

        public AnimatorOverrideController OverrideController
        {
            get
            {
                return animatorOverrideController;
            }
        }

        public float AnimationSpeedMultiplier
        {
            get
            {
                return animator.speed;
            }
        }

        void Update()
        {
            if (!IsDead && navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
            {
                Move(navMeshAgent.desiredVelocity);
            }
            else
            {
                Move(Vector3.zero);
            }
        }

        void OnAnimatorMove()
        {
            // we implement this function to override the default root motion. 
            // this allows us to modify the positional speed before it's applied. 
            if (Time.deltaTime > 0)
            {
                Vector3 velocity = (animator.deltaPosition * movementSpeedMultiplier) / Time.deltaTime;

                // we preserve the existing y part of the current velocity. 
                velocity.y = rigidbodyComponent.velocity.y;
                rigidbodyComponent.velocity = velocity;
            }
        }

        public void SetDestination(Vector3 destination)
        {
            navMeshAgent.SetDestination(destination);
        }

        void Move(Vector3 movement)
        {
            Vector3 localMovement = SetForwardAndTurnMovement(movement);

            ApplyExtraTurnRotation();
            UpdateAnimator(localMovement);
        }

        Vector3 SetForwardAndTurnMovement(Vector3 movement)
        {
            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired direction.
            if (movement.magnitude > MOVE_THRESHOLD)
            {
                movement.Normalize();
            }
            Vector3 localMovement = transform.InverseTransformDirection(movement);
            turnAmount = Mathf.Atan2(localMovement.x, localMovement.z);
            forwardAmount = localMovement.z;
            return localMovement;
        }

        void UpdateAnimator(Vector3 movement)
        {
            animator.SetFloat("Forward", forwardAmount * animatorForwardCap, 0.1f, Time.deltaTime);
            animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);

            // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
            // which affects the movement speed because of the root motion.
            if (movement.magnitude > 0)
            {
                animator.speed = animSpeedMultiplier;
            }
        }

        void ApplyExtraTurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
            transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
        }
    }
}


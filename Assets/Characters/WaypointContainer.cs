﻿using UnityEngine;

namespace RPG.Characters
{
    public class WaypointContainer : MonoBehaviour
    {

        public Vector3 GetWaypoint(int index)
        {
            return transform.GetChild(index).position;
        }

        public int NumberOfWaypoints
        {
            get
            {
                return transform.childCount;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Transform previousChild = null;
            foreach (Transform child in transform)
            {
                Gizmos.color = Color.white;
                if (previousChild)
                {
                    Gizmos.DrawLine(child.position, previousChild.position);
                }
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(child.position, 0.1f);
                previousChild = child;
            }

            Gizmos.color = Color.white;
            if (previousChild)
            {
                Gizmos.DrawLine(previousChild.position, transform.GetChild(0).position);
                Gizmos.DrawSphere(transform.GetChild(0).position, 0.25f);
            }

        }
    }

}
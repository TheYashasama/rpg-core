﻿using UnityEngine;

namespace RPG.Characters
{
    [CreateAssetMenu(menuName = "RPG/WeaponConfig")]
    public class WeaponConfig : ScriptableObject
    {

        public Transform GripTransform;

        [SerializeField] GameObject WeaponPrefab;
        [SerializeField] AnimationClip WeaponAnimation;
        [SerializeField] float timeBetweenAnimationCycles = 0.5f;
        [SerializeField] float attackDelay = 0.1f;
        [SerializeField] float maxAttackRange = 2f;
        [SerializeField] float additionalDamage = 10f;

        public float AdditionalDamage
        {
            get
            {
                return additionalDamage;
            }
        }

        public GameObject Prefab
        {
            get
            {
                return WeaponPrefab;
            }
        }

        public float AttackRange
        {
            get
            {
                return maxAttackRange;
            }
        }

        public float TimeBetweenAnimationCycles
        {
            get
            {
                return timeBetweenAnimationCycles;
            }
        }

        public float AttackDelay
        {
            get
            {
                return attackDelay;
            }
        }

        public AnimationClip Animation
        {
            get
            {
                RemoveAnimationEvents();
                return WeaponAnimation;
            }
        }

        void RemoveAnimationEvents()
        {
            WeaponAnimation.events = new AnimationEvent[0];
        }
    }
}

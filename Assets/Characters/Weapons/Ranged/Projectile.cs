﻿using UnityEngine;

namespace RPG.Characters
{
    public class Projectile : MonoBehaviour
    {

        public int ShooterLayer { get; set; }
        [SerializeField] float projectileSpeed = 10f;
        public float DamageCaused { get; set; }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.gameObject.layer == ShooterLayer) return;
            DamageIDammagable(collision);
        }

        private void DamageIDammagable(Collision collision)
        {
            HealthSystem damagableOther = collision.collider.GetComponent<HealthSystem>();
            if (damagableOther != null)
            {
                damagableOther.TakeDamage(DamageCaused);
                // TODO: manage this 
            }
            Destroy(gameObject);
        }

        public float DefaultSpeed
        {
            get
            {
                return projectileSpeed;
            }
        }
    }
}
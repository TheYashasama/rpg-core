﻿using RPG.Characters;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{
    [ExecuteInEditMode]
    public class WeaponPickupPoint : MonoBehaviour
    {
        [SerializeField] WeaponConfig weaponObject;
        [SerializeField] AudioClip audioClip;
        private AudioSource audioSource;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
            DestroyChildren();
            Instantiate(weaponObject.Prefab, transform).transform.localPosition = Vector3.zero;
        }

        private void Update()
        {
            if(!Application.isPlaying)
            {
                InstantiateWeapon();
            }
        }

        private void InstantiateWeapon()
        {
                DestroyChildren();
                Instantiate(weaponObject.Prefab, transform).transform.localPosition = Vector3.zero;
        }

        private void DestroyChildren()
        {
            foreach (Transform child in transform)
            {
                DestroyImmediate(child.gameObject);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            PlayerControl player = other.gameObject.GetComponent<PlayerControl>();
            if(player)
            {
                if(!audioSource.isPlaying)
                {
                    audioSource.PlayOneShot(audioClip);
                }
                player.GetComponent<WeaponSystem>().EquipWeapon(weaponObject);
                Destroy(gameObject, audioClip.length);
            }
        }
    }
}

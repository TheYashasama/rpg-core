﻿using UnityEngine;

namespace RPG.Characters
{
    public class GroundStompBehaviour : SpecialAbilityBehaviour
    {
        private PlayerControl player;

        private void Start()
        {
            player = FindObjectOfType<PlayerControl>();
        }

        new GroundStompConfig Configuration
        {
            get
            {
                return base.Configuration as GroundStompConfig;
            }
        }

        public override void Use(HealthSystem targetHealthSystem)
        {
            DealDamage();
            PlayParticleEffect();
            PlayAudio();
            PlayAnimation();
        }

        private void DealDamage()
        {
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, Configuration.Range / 2f, Vector3.forward, Configuration.Range / 2f);
            foreach (var hit in hits)
            {
                HealthSystem possibleTarget = hit.collider.GetComponent<HealthSystem>();
                if (possibleTarget != null && possibleTarget != player.GetComponent<HealthSystem>())
                {
                    possibleTarget.TakeDamage(Configuration.Damage);
                }
            }
        }
    }

}
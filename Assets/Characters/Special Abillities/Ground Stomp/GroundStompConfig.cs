﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{
    [CreateAssetMenu(menuName = ("RPG/Special Abilities/Ground Stomp"))]
    public class GroundStompConfig : SpecialAbilityConfig
    {
        [Header("Ground Stomp Specific")]
        [SerializeField]
        float abilityDamagePerTarget = 10f;
        [SerializeField] float range = 5f;

        public override SpecialAbilityBehaviour AttachComponentTo(GameObject gameObjectToAttatchTo)
        {
            return gameObjectToAttatchTo.AddComponent<GroundStompBehaviour>();
        }

        public float Damage
        {
            get
            {
                return abilityDamagePerTarget;
            }
        }

        public float Range
        {
            get
            {
                return range;
            }
        }
    }
}

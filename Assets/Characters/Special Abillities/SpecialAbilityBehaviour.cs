﻿using UnityEngine;

namespace RPG.Characters
{
    public abstract class SpecialAbilityBehaviour : MonoBehaviour
    {
        private const string SPECIAL_ABILITY_TRIGGER = "Special_Ability";
        private const string DEFAULT_SPECIAL_ABILITY = "DEFAULT ABILITY";
        protected SpecialAbilityConfig Configuration;

        public void SetConfiguration(SpecialAbilityConfig newConfig)
        {
            Configuration = newConfig;
        }

        public abstract void Use(HealthSystem targetHealthSystem);

        protected void PlayParticleEffect()
        {
            ParticleSystem particleSystem = Instantiate(Configuration.ParticleSystem, transform).GetComponent<ParticleSystem>();
            particleSystem.Play();
            Destroy(particleSystem.gameObject, particleSystem.main.duration + particleSystem.main.startLifetime.constantMax);
        }

        protected void PlayAudio()
        {
            AudioSource source = GetComponent<AudioSource>();
            source.PlayOneShot(Configuration.RandomAudioClip);
        }

        protected void PlayAnimation()
        {
            Animator animator = GetComponent<Animator>();
            AnimatorOverrideController overrideController = GetComponent<Character>().OverrideController;
            overrideController[DEFAULT_SPECIAL_ABILITY] = Configuration.AbilityAnimation;
            animator.SetTrigger(SPECIAL_ABILITY_TRIGGER);
        }
    }
}

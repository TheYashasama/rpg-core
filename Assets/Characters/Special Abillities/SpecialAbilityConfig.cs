﻿using RPG.Core;
using UnityEngine;

namespace RPG.Characters
{
    public abstract class SpecialAbilityConfig : ScriptableObject
    {
        [Header("Special Ability General")]
        [SerializeField] float energyCost = 10f;
        [SerializeField] GameObject particleSystem;
        [SerializeField] AnimationClip abilityAnimation;
        [SerializeField] SoundSelection audioClips;

        protected SpecialAbilityBehaviour behaviour;

        public abstract SpecialAbilityBehaviour AttachComponentTo(GameObject gameObjectToAttatchTo);

        // public void AttachComponent<T>(GameObject gameObjectToAttatchTo) where T : SpecialAbilityBehaviour
        public void AttachBehaviourTo(GameObject gameObjectToAttatchTo)
        {
            behaviour = AttachComponentTo(gameObjectToAttatchTo);
            behaviour.SetConfiguration(this);
        }

        public GameObject ParticleSystem
        {
            get
            {
                return particleSystem;
            }
        }

        public AnimationClip AbilityAnimation
        {
            get
            {
                RemoveAnimationEvents();
                return abilityAnimation;
            }
        }

        void RemoveAnimationEvents()
        {
            abilityAnimation.events = new AnimationEvent[0];
        }

        public AudioClip  RandomAudioClip
        {
            get
            {
                return audioClips.GetRandomClip();
            }
        }

        public void Use(HealthSystem targetHealthSystem)
        {
            behaviour.Use(targetHealthSystem);
        }

        internal float GetEnergyCost()
        {
            return energyCost;
        }
    }
}

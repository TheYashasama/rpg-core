﻿namespace RPG.Characters
{
    public class SelfHealBehaviour : SpecialAbilityBehaviour
    {
        private PlayerControl player;

        new SelfHealConfig Configuration
        {
            get
            {
                return base.Configuration as SelfHealConfig;
            }
        }

        private void Start()
        {
            player = FindObjectOfType<PlayerControl>();
        }

        public override void Use(HealthSystem targetHealthSystem)
        {
            player.GetComponent<HealthSystem>().Heal(Configuration.HealAmount);
            PlayParticleEffect();
            PlayAudio();
            PlayAnimation();
        }
    }
}

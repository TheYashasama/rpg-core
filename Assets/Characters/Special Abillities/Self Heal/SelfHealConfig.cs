﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{
    [CreateAssetMenu(menuName = ("RPG/Special Abilities/Self Heal"))]
    public class SelfHealConfig :  SpecialAbilityConfig
    {
        [Header("Self Heal Specific")]
        [SerializeField]
        float healAmount = 10f;


        public override SpecialAbilityBehaviour AttachComponentTo(GameObject gameObjectToAttatchTo)
        {
            return gameObjectToAttatchTo.AddComponent<SelfHealBehaviour>();
        }

        public float HealAmount
        {
            get
            {
                return healAmount;
            }
        }
    }
}

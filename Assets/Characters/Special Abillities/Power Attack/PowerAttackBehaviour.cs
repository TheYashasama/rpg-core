﻿namespace RPG.Characters
{
    public class PowerAttackBehaviour : SpecialAbilityBehaviour
    {
        new PowerAttackConfig Configuration
        {
            get
            {
                return base.Configuration as PowerAttackConfig;
            }
        }

        public override void Use(HealthSystem targetHealthSystem)
        {
            targetHealthSystem.TakeDamage(Configuration.Damage);
            PlayParticleEffect();
            PlayAudio();
            PlayAnimation();
        }
    }
}

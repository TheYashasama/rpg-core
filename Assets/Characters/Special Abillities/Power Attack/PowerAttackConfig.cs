﻿using UnityEngine;

namespace RPG.Characters
{
    [CreateAssetMenu(menuName = ("RPG/Special Abilities/Power Attack"))]
    public class PowerAttackConfig :  SpecialAbilityConfig
    {
        [Header("Power Attack Specific")]
        [SerializeField]
        float extraDamage = 10f;

        public float Damage
        {
            get
            {
                return extraDamage;
            }
        }

        public override SpecialAbilityBehaviour AttachComponentTo(GameObject gameObjectToAttatchTo)
        {
            return gameObjectToAttatchTo.AddComponent<PowerAttackBehaviour>();
        }
    }
}

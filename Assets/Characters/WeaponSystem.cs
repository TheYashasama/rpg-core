﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace RPG.Characters
{
    public class WeaponSystem : MonoBehaviour
    {
        private const string ANIM_TRIGGER_ATTACK = "Attack";
        private const string DEFAULT_ATTACK = "DEFAULT ATTACK";

        [SerializeField] float baseDamage = 10f;
        [SerializeField] WeaponConfig equippedWeaponObject;

        Animator animator;

        GameObject weapon;
        float lastHitTime = 0f;
        Character character;
        private HealthSystem currentTarget;

        public float Range
        {
            get
            {
                return equippedWeaponObject.AttackRange;
            }
        }

        private void Start()
        {
            animator = GetComponent<Animator>();
            character = GetComponent<Character>();
            PutWeaponInHand();
        }

        private void Update()
        {
            if (character.IsDead)
            {
                StopAllCoroutines();
            }

            bool targetIsDead = false;
            bool targetIsOutOfRange = false;
            if(currentTarget != null)
            {
                targetIsDead = !currentTarget.IsAlive;
                targetIsOutOfRange = Vector3.Distance(transform.position, currentTarget.transform.position) > equippedWeaponObject.AttackRange;
            }

            if(targetIsDead || targetIsOutOfRange)
            {
                StopAllCoroutines();
            }
        }

        public void EquipWeapon(WeaponConfig weaponObject)
        {
            equippedWeaponObject = weaponObject;
            if (weapon) Destroy(weapon);
            PutWeaponInHand();
        }

        private void PutWeaponInHand()
        {
            weapon = Instantiate(equippedWeaponObject.Prefab, RequestDominantHand(), false);
            weapon.transform.localPosition = equippedWeaponObject.GripTransform.localPosition;
            weapon.transform.localRotation = equippedWeaponObject.GripTransform.localRotation;
            OverrideAnimatorControllerAttackAnimation();
        }

        private void OverrideAnimatorControllerAttackAnimation()
        {
            if (!character.OverrideController)
            {
                Debug.Break();
                Debug.LogAssertion("Please provide " + gameObject + " with Animator Override Controller");
            }
            var overrideController = character.OverrideController;
            animator.runtimeAnimatorController = overrideController;
            overrideController[DEFAULT_ATTACK] = equippedWeaponObject.Animation;
        }

        Transform RequestDominantHand()
        {
            DominantHand[] dominantHands = GetComponentsInChildren<DominantHand>();
            int numberOfDominantHands = dominantHands.Length;
            Assert.IsFalse(numberOfDominantHands <= 0, "No DominantHand found on " + gameObject.name + ", please add one.");
            Assert.IsFalse(numberOfDominantHands > 1, "Multiple DominantHands found on " + gameObject.name + ", please remove all but one.");
            return dominantHands[0].transform;
        }

        public void AttackTarget(HealthSystem targetHealthSystem)
        {
            currentTarget = targetHealthSystem;
            StopAllCoroutines();
            StartCoroutine(AttackTargetRepeatedly());
        }

        IEnumerator AttackTargetRepeatedly()
        {
            while (currentTarget && GetComponent<HealthSystem>().IsAlive && currentTarget.IsAlive)
            {
                AnimationClip animClip = equippedWeaponObject.Animation;
                float clipTime = animClip.length / character.AnimationSpeedMultiplier;

                float timeToWait = equippedWeaponObject.TimeBetweenAnimationCycles + clipTime;
                bool isTimeToHitAgain = Time.time - lastHitTime > timeToWait;
                if (isTimeToHitAgain)
                {
                    AttackTargetOnce();
                    lastHitTime = Time.time;
                }
                yield return new WaitForSeconds(timeToWait);

            }
        }

        void AttackTargetOnce()
        {
            transform.LookAt(currentTarget.transform);
            animator.SetTrigger(ANIM_TRIGGER_ATTACK);
            StartCoroutine(DamageAfterDelay(equippedWeaponObject.AttackDelay));
        }

        IEnumerator DamageAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            currentTarget.TakeDamage(CalculateDamage());
        }

        float CalculateDamage()
        {
            return baseDamage + equippedWeaponObject.AdditionalDamage;
        }

        public bool IsTargetInRange(Vector3 targetPosition)
        {
            return Vector3.Distance(targetPosition, transform.position) < equippedWeaponObject.AttackRange;
        }

        public void StopAttacking()
        {
            animator.StopPlayback();
            StopAllCoroutines();
        }
    }
}


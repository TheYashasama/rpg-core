﻿using RPG.Characters;
using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RPG.Characters
{

    public class HealthSystem : MonoBehaviour
    {
        private const string ANIM_TRIGGER_DEATH = "Death";

        [SerializeField] float maxHealthPoints = 100f;
        [SerializeField] Image healthDisplay;
        [SerializeField] float delayBeforeDeath;

        [SerializeField] SoundSelection deathSounds;
        [SerializeField] SoundSelection damagedSounds;

        Animator animator;
        AudioSource audioSource;
        Character character;

        float currentHealthPoints = 100f;

        public float MaxHealth
        {
            get
            {
                return maxHealthPoints;
            }
        }

        public float CurrentHealth
        {
            get
            {
                return currentHealthPoints;
            }
        }

        public float HealthAsPercentage
        {
            get
            {
                return currentHealthPoints / (float)maxHealthPoints;
            }
        }

        public bool IsAlive
        {
            get
            {
                return HealthAsPercentage >= Mathf.Epsilon;
            }
        }

        void Start()
        {
            GetReferences();
            currentHealthPoints = maxHealthPoints;
        }

        private void GetReferences()
        {
            animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
            character = GetComponent<Character>();
        }

        void Update()
        {
            if (healthDisplay)
            {
                healthDisplay.fillAmount = HealthAsPercentage;
            }
        }

        public void Heal(float amount)
        {
            currentHealthPoints = Mathf.Clamp(currentHealthPoints + amount, 0, maxHealthPoints);
        }

        public void TakeDamage(float damage)
        {
            ReduceHealth(damage);
            if (currentHealthPoints == 0)
            {
                StartCoroutine(KillCharacter());
            }
        }

        private void ReduceHealth(float damage)
        {
            currentHealthPoints = Mathf.Clamp(currentHealthPoints - damage, 0, maxHealthPoints);
            if (damage > 0)
            {
                PlayAudioClip(damagedSounds.GetRandomClip());
            }
        }

        void PlayAudioClip(AudioClip clip, bool overrideSound = false)
        {
            if (!audioSource.isPlaying || overrideSound)
            {
                audioSource.clip = clip;
                audioSource.Play();
            }
        }

        IEnumerator KillCharacter()
        {
            animator.SetTrigger(ANIM_TRIGGER_DEATH);
            character.KillCharacter();
            PlayAudioClip(deathSounds.GetRandomClip(), true);

            PlayerControl player = GetComponent<PlayerControl>();
            if (player && player.isActiveAndEnabled)
            {
                yield return new WaitForSecondsRealtime(audioSource.clip.length * 1.2f);

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                Destroy(gameObject, delayBeforeDeath);
            }

        }
    }

}

﻿using UnityEngine;

namespace RPG.Core
{
    [CreateAssetMenu(menuName = "RPG/FloatPairReference")]
    public class FloatPairReference : ScriptableObject
    {

        public float CurrentValue { get; set; }
        public float MaxValue { get; set; }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

using RPG.CameraUI;
using RPG.Core;

namespace RPG.Characters
{
    public class PlayerControl : MonoBehaviour
    {
        [SerializeField] FloatPairReference HealthReference;
        [SerializeField] FloatPairReference EnergyReference;

        Character character;
        SpecialAbilitySystem specialAbilitySystem;
        WeaponSystem weaponSystem;
        HealthSystem healthSystem;

        public bool IsDead
        {
            get
            {
                return character.IsDead;
            }
        }

        private void Start()
        {
            SetUpComponentReferences();
            RegisterForMouseClick();
        }

        private void SetUpComponentReferences()
        {
            character = GetComponent<Character>();
            specialAbilitySystem = GetComponent<SpecialAbilitySystem>();
            weaponSystem = GetComponent<WeaponSystem>();
            healthSystem = GetComponent<HealthSystem>();
        }

        private void RegisterForMouseClick()
        {

            CameraRaycaster cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
            cameraRaycaster.OnCursorOverWalkableObservers += OnMouseOverWalkable;
            cameraRaycaster.OnCursorOverEnemyObservers += OnMouseOverEnemy;
        }

        void OnMouseOverWalkable(Vector3 position)
        {
            if (Input.GetMouseButtonDown(0))
            {
                weaponSystem.StopAttacking();
                character.SetDestination(position);
            }
        }

        void OnMouseOverEnemy(EnemyAI enemy)
        {
            if (weaponSystem.IsTargetInRange(enemy.transform.position))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    weaponSystem.AttackTarget(enemy.GetComponent<HealthSystem>());
                } else if (Input.GetMouseButtonDown(1))
                {
                    specialAbilitySystem.AttemptSpecialAbility(0, enemy.GetComponent<HealthSystem>());
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    weaponSystem.StopAttacking();
                    Action attack = () => weaponSystem.AttackTarget(enemy.GetComponent<HealthSystem>());
                    StartCoroutine(MoveAndAct(enemy.transform, attack));
                }
                else if (Input.GetMouseButtonDown(1))
                {
                    weaponSystem.StopAttacking();
                    Action usePowerAttack = () => specialAbilitySystem.AttemptSpecialAbility(0, enemy.GetComponent<HealthSystem>());
                    StartCoroutine(MoveAndAct(enemy.transform, usePowerAttack));
                }
            }
        }

        IEnumerator MoveAndAct(Transform target, Action actionToTake)
        {
            while(!weaponSystem.IsTargetInRange(target.position))
            {
                character.SetDestination(target.position);
                yield return new WaitForEndOfFrame();
            }
            actionToTake.Invoke();
        }

        private void Update()
        {
            if (!IsDead)
            {
                ProcessAbilityInput();
                HealthReference.CurrentValue = healthSystem.CurrentHealth;
                HealthReference.MaxValue = healthSystem.MaxHealth;
                EnergyReference.CurrentValue = specialAbilitySystem.CurrentEnergy;
                EnergyReference.MaxValue = specialAbilitySystem.MaxEnergy;
            }
        }

        private void ProcessAbilityInput()
        {
            for (int i = 1; i < specialAbilitySystem.NumberOfAbilites; i++)
            {
                if (Input.GetKeyDown(i.ToString()))
                {
                    specialAbilitySystem.AttemptSpecialAbility(i);
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using UnityEngine;

namespace RPG.Characters
{
    [RequireComponent(typeof(HealthSystem))]
    [RequireComponent(typeof(WeaponSystem))]
    [RequireComponent(typeof(Character))]
    public class EnemyAI : MonoBehaviour
    {
        enum State { Attacking, Idle, Patrolling, Chasing }

        [SerializeField] float chaseDistance = 5f;
        [SerializeField] WaypointContainer waypointContainer;
        [SerializeField] float waypointCycleRadius = 2f;
        [SerializeField] float waitAtWaypointTime = 0.5f;

        PlayerControl player;
        Character character;
        WeaponSystem weaponSystem;

        State currentState = State.Idle;
        private float distanceToPlayer;
        private int nextWaypointIndex;

        float AttackRange
        {
            get
            {
                if(!weaponSystem)
                {
                    weaponSystem = GetComponent<WeaponSystem>();
                }
                return weaponSystem.Range;
            }
        }

        void Start()
        {
            player = GameObject.FindObjectOfType<PlayerControl>();
            character = GetComponent<Character>();
            weaponSystem = GetComponent<WeaponSystem>();
        }

        void Update()
        {
            if(player.IsDead)
            {
                StopAllCoroutines();
            }
            distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

            if (distanceToPlayer <= AttackRange && currentState != State.Attacking)
            {
                StopAllCoroutines();
                StartCoroutine(AttackPlayer());
                return;
            }
            bool InAttackRangeAndAttacking = distanceToPlayer <= AttackRange && currentState == State.Attacking;
            if (distanceToPlayer <= chaseDistance && !InAttackRangeAndAttacking)
            {
                StopAllCoroutines();
                weaponSystem.StopAttacking();
                StartCoroutine(ChasePlayer());
                return;
            }

            bool outOfRange = distanceToPlayer > chaseDistance && distanceToPlayer > AttackRange;
            if (outOfRange && currentState != State.Patrolling)
            {
                StopAllCoroutines();
                weaponSystem.StopAttacking();
                StartCoroutine(Patrol());
                return;
            }
        }

        IEnumerator AttackPlayer()
        {
            currentState = State.Attacking;
            weaponSystem.AttackTarget(player.GetComponent<HealthSystem>());
            yield return new WaitForEndOfFrame(); 
        }

        IEnumerator ChasePlayer()
        {
            currentState = State.Chasing;
            while(distanceToPlayer >= AttackRange)
            {
                character.SetDestination(player.transform.position);
                yield return new WaitForEndOfFrame();
            }
        }

        IEnumerator Patrol()
        {
            currentState = State.Patrolling;
            if(!waypointContainer)
            {
                currentState = State.Idle;
                yield break;
            }

            while (distanceToPlayer >= chaseDistance)
            {
                if (CycleWaypointIfClose())
                {
                    yield return new WaitForSeconds(waitAtWaypointTime);
                }
                Vector3 nextWaypoint = waypointContainer.GetWaypoint(nextWaypointIndex);
                character.SetDestination(nextWaypoint);
                
                yield return new WaitForEndOfFrame();
            }
        }

        private bool CycleWaypointIfClose()
        {
            float distanceToWaypoint = Vector3.Distance(waypointContainer.GetWaypoint(nextWaypointIndex), transform.position); 
            if(distanceToWaypoint <= waypointCycleRadius)
            {
                nextWaypointIndex = (nextWaypointIndex + 1) % waypointContainer.NumberOfWaypoints;
                return true;
            }
            return false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, AttackRange);
            //Gizmos.DrawSphere(projectileSocket.transform.position, 0.1f);

        }
    }


}
﻿using RPG.Core;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Characters
{
    public class SpecialAbilitySystem : MonoBehaviour
    {
        [SerializeField] Image energyDisplay;
        [SerializeField] float maxEnergyPoints = 100f;
        [SerializeField] float energyRegenerationPerSecond = 1f;
        [SerializeField] SoundSelection outOfEnergySounds;
        [SerializeField] SpecialAbilityConfig[] abilities;

        AudioSource audioSource;

        float currentEnergyPoints;

        public float CurrentEnergy
        {
            get
            {
                return currentEnergyPoints;
            }
        }

        public float MaxEnergy
        {
            get
            {
                return maxEnergyPoints;
            }
        }

        float EnergyAsPercentage
        {
            get
            {
                return currentEnergyPoints / maxEnergyPoints;
            }
        }

        public int NumberOfAbilites
        {
            get
            {
                return abilities.Length;
            }
        }

        bool IsEnergyAvailable(float amount)
        {
            return amount <= currentEnergyPoints;
        }

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
            currentEnergyPoints = maxEnergyPoints;
            UpdateEnergyBar();
            AttachAbilityComponents();
        }

        void AttachAbilityComponents()
        {
            foreach (var ability in abilities)
            {
                ability.AttachBehaviourTo(gameObject);
            }
        }

        void Update()
        {
            if(maxEnergyPoints != currentEnergyPoints)
            {
                RegenerateEnergyPerSecond();
            }
        }

        void UpdateEnergyBar()
        {
            if(energyDisplay)
            {
                energyDisplay.fillAmount = EnergyAsPercentage;
            }
        }

        void RegenerateEnergyPerSecond()
        {
            currentEnergyPoints = Mathf.Clamp(currentEnergyPoints + energyRegenerationPerSecond * Time.deltaTime, 0, maxEnergyPoints);
            UpdateEnergyBar();
        }

        public void ConsumeEnergy(float engeryCost)
        {
            currentEnergyPoints = Mathf.Clamp(currentEnergyPoints - engeryCost, 0, maxEnergyPoints);
            UpdateEnergyBar();
        }

        public void AttemptSpecialAbility(int index, HealthSystem targetHealthSystem = null)
        {
            SpecialAbilitySystem energyComponent = GetComponent<SpecialAbilitySystem>();
            if (energyComponent.IsEnergyAvailable(abilities[index].GetEnergyCost()))
            {
                energyComponent.ConsumeEnergy(abilities[index].GetEnergyCost());
                abilities[index].Use(targetHealthSystem);
            } else
            {
                audioSource.PlayOneShot(outOfEnergySounds.GetRandomClip());
            }
        }
    }
}

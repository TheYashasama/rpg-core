﻿using UnityEngine;
namespace RPG.Core
{
    public class SpinMe : MonoBehaviour
    {

        [SerializeField] float xRotationsPerMinute = 1f;
        [SerializeField] float yRotationsPerMinute = 1f;
        [SerializeField] float zRotationsPerMinute = 1f;

        void Update()
        {
            float xDegreesPerFrame = ComputeDegreesPerFrame(xRotationsPerMinute);
            transform.RotateAround(transform.position, transform.right, xDegreesPerFrame);

            float yDegreesPerFrame = ComputeDegreesPerFrame(yRotationsPerMinute);
            transform.RotateAround(transform.position, transform.up, yDegreesPerFrame);

            float zDegreesPerFrame = ComputeDegreesPerFrame(zRotationsPerMinute);
            transform.RotateAround(transform.position, transform.forward, zDegreesPerFrame);
        }

        float ComputeDegreesPerFrame(float rotationsPerMinute)
        {
            return Time.deltaTime / 60 * 360 * rotationsPerMinute;
        }
    }

}

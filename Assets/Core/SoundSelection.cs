﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    [CreateAssetMenu(menuName = "RPG/Sound Selection")]
    public class SoundSelection : ScriptableObject
    {

        [SerializeField] AudioClip[] audioClips;

        public AudioClip GetRandomClip()
        {
            return audioClips[Random.Range(0, audioClips.Length)];
        }
    }
}

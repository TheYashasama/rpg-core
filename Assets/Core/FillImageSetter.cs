﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Core
{
    public class FillImageSetter : MonoBehaviour
    {
        public FloatPairReference PairReference;
        public Image FillImage;

        private void OnGUI()
        {
            FillImage.fillAmount = PairReference.CurrentValue / PairReference.MaxValue;
        }
    }
}
